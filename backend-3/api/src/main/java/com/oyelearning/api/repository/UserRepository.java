package com.oyelearning.api.repository;

import org.springframework.data.repository.CrudRepository;
import com.oyelearning.api.entities.UserEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, String> {

}
