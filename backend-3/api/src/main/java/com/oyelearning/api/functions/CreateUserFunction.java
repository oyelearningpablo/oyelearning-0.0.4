package com.oyelearning.api.functions;



import java.util.UUID;
import java.util.function.Function;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.oyelearning.api.entities.UserEntity;
import com.oyelearning.api.repository.UserRepository;
import com.oyelearning.api.request.APIGatewayRequest;
@Component
public class CreateUserFunction implements Function<APIGatewayRequest,UserEntity> {
    @Autowired
    private UserRepository userRepository;
    @Override
    public UserEntity apply(APIGatewayRequest userRequest) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        UserEntity userEntity = modelMapper.map(userRequest.getUserRequest(), UserEntity.class);
        userEntity.setId(UUID.randomUUID().toString());
        userRepository.save(userEntity);
        return userEntity;
    }
}
