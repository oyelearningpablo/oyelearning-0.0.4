package com.oyelearning.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author skargopolov
 */
@RestController
@RequestMapping("users")
public class UserController {

    @GetMapping("/status/check")
    public String status() {
        return "working";
    }
}
