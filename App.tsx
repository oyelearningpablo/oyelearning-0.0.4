import React from "react";
import { Provider } from "react-native-paper";
import MainApp from "./src/index";
import { theme } from "./src/core/theme";
import { Provider as ReduxProvider } from "react-redux";

import store from './src/store/store';

export default function App() {
  return (
    <ReduxProvider store={store}>
      
        <Provider theme={theme}>
          <MainApp />
        </Provider>
      
    </ReduxProvider>
  );
}
