-Trabajo con librería "flex-layout" (también asociada a esa versión de angular). Tanto uso de flex como de grid. (De cara a maquetación responsiva)

-SCSS, explotación del uso de variables, imports y funciones.

-Uso e integración correcta de temas personalizados de angular-material. (Definición de temas, y uso de mixing, de forma escalable sin apoyarse completamente en fichero global). Adicionalmente, apoyo en paleta de colores adicionales a las 3 manejadas por defecto. Los componentes han de mantener el aislamiento de su css.
