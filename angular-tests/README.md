Buenas tardes Pablo,

En primer lugar, disculpa la demora en indicarte el stack tecnológico a profundizar. No hemos podido cerrarlo antes con el cliente y quería enviarte la información lo más concreta posible.

Nos viene impuesto, hasta cierto punto, por el cliente. Como base tiene que ser Angular 8 y la versión de Angular material asociada.

Con eso en mente, sería interesante que indagases en:

-Trabajo con librería "flex-layout" (también asociada a esa versión de angular). Tanto uso de flex como de grid. (De cara a maquetación responsiva)

-SCSS, explotación del uso de variables, imports y funciones.

-Uso e integración correcta de temas personalizados de angular-material. (Definición de temas, y uso de mixing, de forma escalable sin apoyarse completamente en fichero global). Adicionalmente, apoyo en paleta de colores adicionales a las 3 manejadas por defecto. Los componentes han de mantener el aislamiento de su css.

-Diseño atómico aplicado a componentes Angular.
-Proyección mediante ng-template en función del dispositivo. Aquí asume 2 tipos de presentaciones bastante distintas, una para movil-tablet y otra para escritorio.

-Implementación de stepper personalizado utilizando el CDK de material. Al menos, entender cómo se realizaría.

-Internacionalización mediante ngx-translate.

-Test unitarios mediante Jasmine/Karma

-Encapsulamiento de componentes de formulario. Es decir, componentes propios que se integren con formularios reactivos y que internamente puedan contener componentes existentes de angular material. De modo que sean autocontenidos en cuanto a mensajes de error y forma de presentación. En principio, estos componentes contendrán su propio mat-form-field, de modo que solo sea necesario vincularlo al FormControl apropiado.

-Conceptos del patrón redux en Angular. NgRx, uso de store y posiblemente aplicación de efectos.

-Conceptos de accesibilidad AA. Implicaciones del contraste, lectores de pantalla y foco.

-Conceptos de funcionamiento de un bridge javascript para la intercomunicación de un webview (navegador embebido en aplicación nativa) con la web.

-Aplicación de CSS especial para la impresión. Es decir, @media print.

-Uso de BottomSheet y Dialog, desde servicios.

-Propagación de errores en rxjs mediante errores personalizados, extensión de Error.

-Interceptors. No solo para añadir información a las peticiones sino para el tratamiento de errores.

-Guards. Control de acceso y redirección si aplica.

-Pipes. Transformación de la información a presentar.

-Directives. Tanto de contenido, como de aplicación de funcionalidad adicional.

-Herencia, extensión y poliformismo de tipos en TS. Reutilización de código y aseguramiento del tipado.

-Manejo de fechas mediante moment.js, especialmente integrado con angular.

-Ciclo de vida de la inyección de dependencias para componentes hijos. Es decir, metadato "provide" de un componente, aportando o bien una factory o una instancia existente.

-RXJS, BehaviourSubject vs Subject, uso de operadores map, switchMap y tap. También la gestión de observadores (next, error, complete), así como el ciclo de vida de las suscripciones.
