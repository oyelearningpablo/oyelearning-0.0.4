const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    "/dev",
    createProxyMiddleware({
      target: "https://gpv61qzunl.execute-api.us-east-1.amazonaws.com",
      changeOrigin: true,
    })
  );
};
