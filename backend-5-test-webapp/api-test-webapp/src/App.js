import axios from "axios";
import React, { useState } from "react";

function App() {
  const [responseData, setResponseData] = useState(null);

  const apiGatewayResource = "/dev/person";
  const handleGet = async () => {
    const id = 1;
    try {
      const response = await axios.get(`${apiGatewayResource}?id=${id}`);
      console.log(response.data);
      setResponseData(response.data);
    } catch (error) {
      console.error(error);
      setResponseData(error);
    }
  };

  const handlePost = async () => {
    try {
      const id = 1;
      const data = {
        id: id,
        firstName: "John",
        lastName: "Doe",
        age: 30,
        address: "123 Main St",
      };
      const queryParams = {
        id: id,
      };
      const response = await axios.post(`${apiGatewayResource}`, data, {
        params: queryParams,
      });
      console.log(response.data);
      setResponseData(response.data);
    } catch (error) {
      console.error(error);
      setResponseData(error);
    }
  };

  const handlePut = async () => {
    const id = 1;
    try {
      const data = {
        id: id,
        firstName: "Jane",
        lastName: "Doe",
        age: 30,
        address: "123 Main St",
      };
      const response = await axios.put(`${apiGatewayResource}?id=${id}`, data);
      console.log(response.data);
      setResponseData(response.data);
    } catch (error) {
      console.error(error);
      setResponseData(error);
    }
  };

  const handleDelete = async () => {
    const id = 1;
    try {
      const response = await axios.delete(`${apiGatewayResource}?id=${id}`);
      console.log(response.data);
      setResponseData(response.data);
    } catch (error) {
      console.error(error);
      setResponseData(error);
    }
  };

  return (
    <div>
      <button onClick={handleGet}>GET</button>
      <button onClick={handlePut}>PUT</button>
      <button onClick={handlePost}>POST</button>
      <button onClick={handleDelete}>DELETE</button>
      {responseData && (
        <div>
          <p>{JSON.stringify(responseData)}</p>
        </div>
      )}
    </div>
  );
}

export default App;
