package com.oyelearning.api.apigateway;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;

public class APIDemoHandlerV2 implements RequestStreamHandler {

	private DynamoDBMapper mapper = new DynamoDBMapper(AmazonDynamoDBClientBuilder.defaultClient());

	/*
	 * curl -X POST -d '{"id":1, "firstName":"John", "lastName":"Doe", "age":30,
	 * "address": "123 Main St"}' -H "Content-Type: application/json"
	 * https://your_api_gateway_url/dev/person curl -X GET
	 * https://gpv61qzunl.execute-api.us-east-1.amazonaws.com/dev/person?id=1 curl
	 * -X PUT -d '{"id":1, "firstName":"Jane", "lastName":"Doe", "age":30,
	 * "address": "123 Main St"}' -H "Content-Type: application/json"
	 * https://your_api_gateway_url/dev/person?id=1 curl -X DELETE
	 * https://your_api_gateway_url/dev/person?id=1
	 * 
	 */
	@Override
	public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		JSONObject responseJson = new JSONObject();

		try {
			JSONObject event = (JSONObject) JSONValue.parse(reader);

			if (event.containsKey("httpMethod") && event.containsKey("queryStringParameters")) {
				String httpMethod = (String) event.get("httpMethod");
				Map<String, String> queryParams = (Map<String, String>) event.get("queryStringParameters");

				if (queryParams.containsKey("id")) {
					int id = Integer.parseInt(queryParams.get("id"));
					if (httpMethod.equals("POST")) {
						responseJson = handlePostRequest(event, id);
					} else if (httpMethod.equals("GET")) {
						responseJson = handleGetRequest(id);
					} else if (httpMethod.equals("PUT")) {
						responseJson = handlePutRequest(event, id);
					} else if (httpMethod.equals("DELETE")) {
						responseJson = handleDeleteRequest(id);
					} else {
						responseJson.put("statusCode", 405);
						responseJson.put("body", "Unsupported HTTP method: " + httpMethod);
					}
				} else {
					responseJson.put("statusCode", 400);
					responseJson.put("body", "Invalid request format: 'id' query parameter is missing");
				}
			} else {
				responseJson.put("statusCode", 400);
				responseJson.put("body", "Invalid request format");
			}
		} catch (Exception e) {
			responseJson.put("statusCode", 500);
			responseJson.put("body", "Exception: " + e.getMessage());
		}

		OutputStreamWriter writer = new OutputStreamWriter(output, "UTF-8");
		writer.write(responseJson.toString());
		writer.close();
	}

	/*
	 * CRUDPersonAPIGWPOST { "httpMethod": "POST", "queryStringParameters": { "id":
	 * "1" }, "body":
	 * "{\"id\":1, \"firstName\":\"John\", \"lastName\":\"Doe\", \"age\":30, \"address\": \"123 Main St\"}"
	 * }
	 * 
	 */
	private JSONObject handlePostRequest(JSONObject event, int id) {
		if (id >= 0) {
			Person person = new Person();
			Person personParsed = person.fromJson(event.get("body").toString());
			personParsed.setId(id);
			mapper.save(personParsed);
			JSONObject responseJson = new JSONObject();
			responseJson.put("statusCode", 201);
			responseJson.put("body", "Person created:\n" + personParsed.toString());
			return responseJson;
		} else {
			JSONObject responseJson = new JSONObject();
			responseJson.put("statusCode", 400);
			responseJson.put("body", "Invalid request format: 'id' query parameter must be a non-negative integer");
			return responseJson;
		}
	}

	/*
	 * CRUDPersonAPIGWGET { "httpMethod": "GET", "queryStringParameters": { "id":
	 * "1" } }
	 * 
	 */

	private JSONObject handleGetRequest(int id) {
		if (id >= 0) {
			Person person = mapper.load(Person.class, id);
			if (person != null) {
				JSONObject responseJson = new JSONObject();
				responseJson.put("statusCode", 200);
				responseJson.put("body", person.toJson());
				return responseJson;
			} else {
				JSONObject responseJson = new JSONObject();
				responseJson.put("statusCode", 404);
				responseJson.put("body", "Person not found with id: " + id);
				return responseJson;
			}
		} else {
			JSONObject responseJson = new JSONObject();
			responseJson.put("statusCode", 400);
			responseJson.put("body", "Invalid request format: 'id' query parameter must be a non-negative integer");
			return responseJson;
		}
	}

	/*
	 * CRUDPersonAPIGWPUT { "httpMethod": "PUT", "queryStringParameters": { "id": 1
	 * }, "body":
	 * "{\"id\":1, \"firstName\":\"Jane\", \"lastName\":\"Doe\", \"age\":30, \"address\": \"123 Main St\"}"
	 * }
	 * 
	 */
	private JSONObject handlePutRequest(JSONObject event, int id) {
		if (id >= 0) {
			Person person = mapper.load(Person.class, id);
			if (person != null) {
				Person personParsed = person.fromJson(event.get("body").toString());
				personParsed.setId(id);
				mapper.save(personParsed);
				JSONObject responseJson = new JSONObject();
				responseJson.put("statusCode", 200);
				responseJson.put("body", "Person updated from:\n" + person.toString() + " to \n"+ personParsed.toString());
				return responseJson;
			} else {
				JSONObject responseJson = new JSONObject();
				responseJson.put("statusCode", 404);
				responseJson.put("body", "Person not found with id: " + id);
				return responseJson;
			}
		} else {
			JSONObject responseJson = new JSONObject();
			responseJson.put("statusCode", 400);
			responseJson.put("body", "Invalid request format: 'id' query parameter must be a non-negative integer");
			return responseJson;
		}
	}

	private JSONObject handleDeleteRequest(int id) {
		if (id >= 0) {
			Person person = mapper.load(Person.class, id);
			if (person != null) {
				mapper.delete(person);
				JSONObject responseJson = new JSONObject();
				responseJson.put("statusCode", 200);
				responseJson.put("body", "Person deleted:\n" + person.toString());
				return responseJson;
			} else {
				JSONObject responseJson = new JSONObject();
				responseJson.put("statusCode", 404);
				responseJson.put("body", "Person not found with id: " + id);
				return responseJson;
			}
		} else {
			JSONObject responseJson = new JSONObject();
			responseJson.put("statusCode", 400);
			responseJson.put("body", "Invalid request format: 'id' query parameter must be a non-negative integer");
			return responseJson;
		}
	}
}