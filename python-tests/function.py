import requests
import datetime
from bs4 import BeautifulSoup
from requests.exceptions import ProxyError, ConnectionError, Timeout

# Define the filename of the whitelist file
WHITELIST_FILE = "whitelist.txt"

def load_proxies_from_whitelist():
    # Load the list of working proxies from the whitelist file
    try:
        with open(WHITELIST_FILE, "r") as f:
            return [line.strip() for line in f]
    except FileNotFoundError:
        return []

def write_proxies_to_whitelist(proxy_list):
    # Save the updated list of working proxies to the whitelist file
    with open(WHITELIST_FILE, "w") as f:
        for proxy in proxy_list:
            f.write(proxy + "\n")

def scrape_noizagenda_using_proxy(proxy_url, try_count):
    # Use the extracted proxy for making web requests to the target URL
    today = datetime.date.today()
    formatted_date = today.strftime('%Y-%m-%d')
    web_url = f"https://www.noizagenda.com/agenda?d={formatted_date}"
    session = requests.Session()
    session.proxies = {
        "http": proxy_url,
        "https": proxy_url
    }
    session.mount("https://", requests.adapters.HTTPAdapter(max_retries=10))
    headers = {'User-Agent': 'Mozilla/5.0'}
    try:
        print(f"Connecting to {proxy_url}...", end="")
        response = session.get(web_url, headers=headers, timeout=10, verify=True)
        print("Connected!")
        # Use BeautifulSoup to extract and format the required data from the response object
        soup = BeautifulSoup(response.content, 'html.parser')
        formatted_events = []
        eventos = soup.find_all('div', class_='agenda-item')
        for evento in eventos:
            nombre = evento.find('span', itemprop='name').text.strip()
            fecha = evento.find('span', itemprop='startDate').text.strip()
            lugar = evento.find('span', itemprop='location').find('span', itemprop='name').text.strip()
            direccion = evento.find('span', itemprop='location').find('span', itemprop='addressLocality').text.strip()
            precio = evento.find('span', itemprop='offers').find('span', itemprop='price').text.strip()

            formatted_event = {
                'nombre': nombre,
                'fecha': fecha,
                'lugar': lugar,
                'direccion': direccion,
                'precio': precio
            }
            formatted_events.append(formatted_event)
        print("\nScraping complete!")
        return formatted_events
    except (ProxyError, ConnectionError, Timeout) as e:
        print(f"\nError with proxy {proxy_url} on try {try_count}: {str(e)}")
        return None
    except Exception as e:
        print(f"Error: {str(e)}")
        return None


def scrape_elcorreo_using_proxy(proxy_url, try_count):
    # Use the extracted proxy for making web requests to the target URL    
    web_url = "https://agenda.elcorreo.com/eventos/bilbao/listado.html?pag=1"
    session = requests.Session()
    session.proxies = {
        "http": proxy_url,
        "https": proxy_url
    }
    session.mount("https://", requests.adapters.HTTPAdapter(max_retries=10))
    headers = {'User-Agent': 'Mozilla/5.0'}
    try:
        print(f"Connecting to {proxy_url}...", end="")
        response = session.get(web_url, headers=headers, timeout=10, verify=True)
        print("Connected!")
        # Use BeautifulSoup to extract and format the required data from the response object
        soup = BeautifulSoup(response.content, 'html.parser')
        formatted_events = []
        eventos = soup.find_all('article')
        for evento in eventos:
            nombre = evento.find('h2').text +' descripcion: '+ evento.find('div', {'class': 'voc-agenda-noticia'}).text.strip()
            fecha = evento.find('span', {'class': 'voc-agenda-dia'}).text + ' hora: '+ evento.find_all('span', {'class': 'voc-agenda-dia'})[1].text
            lugar =  evento.find('span', {'class': 'voc-agenda-localidad'}).text
            direccion = ''
            precio = ''

            formatted_event = {
                'nombre': nombre,
                'fecha': fecha,
                'lugar': lugar,
                'direccion': direccion,
                'precio': precio
            }
            formatted_events.append(formatted_event)
        print("\nScraping complete!")
        return formatted_events
    except (ProxyError, ConnectionError, Timeout) as e:
        print(f"\nError with proxy {proxy_url} on try {try_count}: {str(e)}")
        return None
    except Exception as e:
        print(f"Error: {str(e)}")
        return None


def main():
    # Load the list of working proxies from the whitelist file
    whitelist = load_proxies_from_whitelist()
    if whitelist:
        print("Whitelisted proxies:")
        print(whitelist)

    # API call to retrieve a list of proxies
    url = "https://api.proxyscrape.com/v2/?request=displayproxies&protocol=http&timeout=10000&country=all&ssl=all&anonymity=all"
    response = requests.get(url)
    proxy_list = response.text.strip().split('\r\n')[:100]

    # Try the proxies in the whitelist first, then new proxies from proxyscrape.com
    ######### USANDO LA LISTA DE PROXIES!!!
    #for i, proxy in enumerate(whitelist + proxy_list):
    for i, proxy in enumerate(whitelist):
        formatted_events = scrape_noizagenda_using_proxy(proxy, i+1)
        if formatted_events:
            print('Eventos de noizagenda')
            print(formatted_events)
            # write the working proxy to the whitelist file
            ######### ALMACENANDO LOS PROXIES VALIDOS
            #write_proxies_to_whitelist([proxy] + whitelist)
            break
    for i, proxy in enumerate(whitelist):
        formatted_events = scrape_elcorreo_using_proxy(proxy, i+1)
        if formatted_events:
            print('Eventos de el correo')
            print(formatted_events)
            # write the working proxy to the whitelist file
            ######### ALMACENANDO LOS PROXIES VALIDOS
            #write_proxies_to_whitelist([proxy] + whitelist)
            break


if __name__ == '__main__':
    main()