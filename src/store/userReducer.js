import { ADD_USER, DELETE_USER, DID_USER, UPDATE_EVALUACION, SET_VIDEO_LOADING } from "./userTypes";
const initialState = {
  inicioScreen: {
    header: "Comencemos!",
    rpnCard1: {
      titulo: "Acceso libre",
      subtitulos: ["1 usuario", "Soporte Basico"],
      recursoImagen: "libre",
      tituloNavegacion: "Acceder",
    },
    rpnCard2: {
      titulo: "Acceso Premium",
      subtitulos: ["Acceso a RRSS", "Evolución"],
      tituloNavegacion: "Suscribirme",
    },
    button1: {
      titulo: "Evolución",
    },
    button2: {
      titulo: "RRSS",
    },
  },
  explicacionBreveScreen: {
    header: "IKASI",
    texto1:
      "Ikasi es una estructura de entrenamiento y evaluación cognitiva, que persigue la potenciación de las Funciones Ejecutivas en la etapa de 3 a 6 años. Se basa en los siguientes pilares esenciales:",
    item1: "* Neuropsicopedagogía",
    item2: "* Los juegos interactivos con el adulto",
    item3: "* La comunicación eficiente en la instrucción.",
    item4: "* Mindfulness",
  },
  menuDimensionalScreen: {
    header: "Menú Dimensional",
    lista: [
      {
        title: "Dimensión Comunicativa",
        icon: "dimension-comunicativa",
        ruta: "DimensionComunicativaScreen",
        tag: "dimensionComunicativa",
      },
      {
        title: "Dimensión Naturalista",
        icon: "dimension-naturalista",
        ruta: null,
        tag: "dimensionNaturalista",
      },
      {
        title: "Dimensión Matemática",
        icon: "dimension-matematica",
        ruta: null,
        tag: "dimensionMatematica",
      },
      {
        title: "Dimensión Visio-Espacial",
        icon: "dimension-visioespacial",
        ruta: null,
        tag: "dimensionVisioEspacial",
      },
      {
        title: "Dimensión Artístico-Musical",
        icon: "artistico-musical",
        ruta: null,
        tag: "dimensionArtisticoMusical",
      },
      {
        title: "Dimensión Cinestésica",
        icon: "dimension-cinestesica",
        ruta: null,
        tag: "dimensionCinestesica",
      },
      {
        title: "Dimensión Emocional",
        icon: "dimension-emocional",
        ruta: null,
        tag: "dimensionEmocional",
      },
    ],
  },
  dimensionComunicativa: {
    header: "Dimensión Comunicativa",
    rpnCard1: {
      texto1: "Objetivos:",
      item1: "* Recordar trabalenguas",
      item2: "* Reflexionar sobre la velocidad y el aprendizaje",
      item3: "* Resolver adivinanzas cargadas en la memoria visual",
      item4: "* Flexibilizar las respuestas posibles",
      item5: "* Liderar positivamente",
    },
    sesion1: {
      header: "Sesion #1",
      listaPuntosAtencion: [
        {
          id: 1,
          texto: "¿Qué vamos a aprender? Metaatención, planificación.",
          segundo: 43,
        },
        {
          id: 2,
          texto:
            "Mirada abajo, busqueda de info. Excitación neuronal, paciencia!!!",
          segundo: 192,
        },
        {
          id: 3,
          texto: "¿Para qué te va servir?. Metacognición / significante.",
          segundo: 210,
        },
        {
          id: 4,
          texto: "Mnemotecnia.",
          segundo: 245,
        },
        {
          id: 5,
          texto: "Los dedos ayudan al recuerdo",
          segundo: 265,
        },
        {
          id: 6,
          texto: "Metacognición final",
          segundo: 445,
        },
        {
          id: 7,
          texto: "Planificación siguiente sesión",
          segundo: 486,
        },
      ],
      preguntasEvaluacion: [
        {
          dia: "lunes",
          numeroDiaSemana: 1,
          funcionCognitiva: "Evaluación Sesión #1",
          indicadores: [
            {
              pregunta: "Número de trabalenguas recordados sin ayuda",
              info: "Número de trabalenguas que ha recitado sin ninguna ayuda. 0, 1, 2 o 3.",
              respuestas: ["Mal", "Regular", "Excelente"],
            },
            {
              pregunta: "Nivel de atención sostenida",
              info: "Cómo le has visto de atento e interesado, en una escala de 1 a 5.",
              respuestas: [
                "Mal",
                "Mediocre",
                "Regular",
                "Notable",
                "Excelente",
              ],
            },
            {
              pregunta: "Reflexión metacognitiva lograda",
              info: "Cómo valoras las reflexiones evocadas, en respuesta a las preguntas realizadas en la tarea, en una escala de 1 a 5.",
              respuestas: [
                "Mal",
                "Mediocre",
                "Regular",
                "Notable",
                "Excelente",
              ],
            },
            {
              pregunta: "Calidad del tiempo compartido",
              info: "¿Cómo evalúas la calidad del tiempo compartido? Ten en cuenta: satisfacción de ambos, aprendizaje del participante (es@ es tu hij@),  y formación del instructor (es@ eres tú).",
              respuestas: [
                "Mal",
                "Mediocre",
                "Regular",
                "Notable",
                "Excelente",
              ],
            },
          ],
        },
      ],
    },
  },
  users: [
    { user: "HTML I", done: true, id: "1" },
    { user: "CSS", done: true, id: "2" },
    { user: "Responsive design", done: true, id: "3" },
  ],
  evaluacion: [],
  videoLoading: false
};
const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_USER:
      return {
        ...state,
        users: [
          ...state.users,
          {
            user: action.payload,
            done: false,
            id: Math.random().toString(),
          },
        ],
      };
    case DELETE_USER:
      return {
        ...state,
        users: state.users.filter((item) => item.id != action.payload),
      };
    case DID_USER:
      return {
        ...state,
        users: state.users.map((item) => {
          if (item.id != action.payload) {
            return item;
          }
          return {
            ...item,
            done: true,
          };
        }),
      };
      break;
    case UPDATE_EVALUACION:     
      let newArray = [...state.evaluacion]; //Copying state array
      newArray[action.payload.index] = action.payload.rating;
      return {
        ...state,
        evaluacion: [...newArray]
      };
      break;
    case SET_VIDEO_LOADING:
      return {
        ...state,
        videoLoading: action.payload.loading
      }
    default:
      return state;
  }
};
export default userReducer;
