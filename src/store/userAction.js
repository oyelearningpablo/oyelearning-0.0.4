import { ADD_USER, DELETE_USER, DID_USER, UPDATE_EVALUACION,SET_VIDEO_LOADING } from "./userTypes";
export const addTask = (user) => ({
  type: ADD_USER,
  payload: user,
});
export const deleteTask = (id) => ({
  type: DELETE_USER,
  payload: id,
});
export const didTask = (id) => ({
  type: DID_USER,
  payload: id,
});

export const updateEvaluacion = (rating, index) => ({
  type: UPDATE_EVALUACION,
  payload: {
    rating,
    index
  }
});

export const setVideoLoading = (loading) => ({
  type: SET_VIDEO_LOADING,
  payload:{
    loading
  }
})
