import { configureFonts, DefaultTheme } from "react-native-paper";
const primary = "hsl(205, 47%, 70%)"; // Medium blue

// To make the color more light, increase the lightness value
const lightPrimary = "hsl(205, 47%, 56%)"; // Lighter blue
export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: primary,
    secondary: lightPrimary, // Light blue
    error: "#f44336", // Bright red
    background: "#ffffff", // White
    text: "#000000", // Black
    border: "#d3d3d3", // Grey
    accent: "blue",
  },
  fonts: configureFonts({
    web: {
      regular: {
        fontFamily: "sans-serif",
        fontWeight: "normal",
      },
      medium: {
        fontFamily: "sans-serif-medium",
        fontWeight: "normal",
      },
      light: {
        fontFamily: "sans-serif-light",
        fontWeight: "normal",
      },
      thin: {
        fontFamily: "sans-serif-thin",
        fontWeight: "normal",
      },
    },
    ios: {
      regular: {
        fontFamily: "sans-serif",
        fontWeight: "normal",
      },
      medium: {
        fontFamily: "sans-serif-medium",
        fontWeight: "normal",
      },
      light: {
        fontFamily: "sans-serif-light",
        fontWeight: "normal",
      },
      thin: {
        fontFamily: "sans-serif-thin",
        fontWeight: "normal",
      },
    },
    android: {
      regular: {
        fontFamily: "Roboto-Regular",
        fontWeight: "normal",
      },
      medium: {
        fontFamily: "Roboto-Medium",
        fontWeight: "normal",
      },
      light: {
        fontFamily: "Roboto-Light",
        fontWeight: "normal",
      },
      thin: {
        fontFamily: "Roboto-Thin",
        fontWeight: "normal",
      },
      bold: {
        fontFamily: "Roboto-Bold",
        fontWeight: "normal",
      },
    },
  }),
  spacing: ({ type, amount }) => {
    if (type === "margin") {
      return 8 * amount; // Change the number to increase or decrease the spacing
    } else if (type === "padding") {
      return 8 * (amount / 2); // Divide the amount by 2 for padding (React Native automatically doubles padding values for both sides)
    } else {
      return 0;
    }
  },
};
