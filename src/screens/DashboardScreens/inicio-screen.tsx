import React, { memo, useEffect, useState } from "react";
import { Animated, ScrollView, StyleSheet, View } from "react-native";
import { useSelector } from "react-redux";
import Background from "../../components/Background";
import Button from "../../components/Button";
import Header from "../../components/Header";
import RNPCard from "../../components/RNPCard";
import { height, width } from "../../core/utils";
import { Navigation } from "../../types";

type Props = {
  navigation: Navigation;
};

const InicioScreen = ({ navigation }: Props) => {
  const [springAnimation, setSpringAnimation] = useState(new Animated.Value(1));
  const inicioScreen = useSelector((state: any) => state.inicioScreen);
  useEffect(() => {
    springAnimation.setValue(0);
    Animated.spring(springAnimation, {
      toValue: 1,
      friction: 8,
      useNativeDriver: true,
    }).start();
  }, []);

  return (
    <Background>
      <Header>{inicioScreen.header}</Header>

      <ScrollView>
        <View style={styles.cardcontainer}>
          <RNPCard
            titulo={inicioScreen.rpnCard1.titulo}
            subtitulos={inicioScreen.rpnCard1.subtitulos}
            navegacion={() => navigation.navigate("ExplicacionBreveScreen")}
            recursoImagen={inicioScreen.rpnCard1.recursoImagen}
            tituloNavegacion={inicioScreen.rpnCard1.tituloNavegacion}
          />
        </View>
        <View style={styles.cardcontainer}>
          <RNPCard
            titulo={inicioScreen.rpnCard2.titulo}
            subtitulos={inicioScreen.rpnCard2.subtitulos}
            tituloNavegacion={inicioScreen.rpnCard2.tituloNavegacion}
          />
        </View>
        <Button
          mode="contained"
          onPress={() => navigation.navigate("InicioScreen")}
          icon="rocket"
        >
          {inicioScreen.button1.titulo}
        </Button>

        <Button
          mode="contained"
          onPress={() => navigation.navigate("InicioScreen")}
          icon="facebook"
        >
          {inicioScreen.button2.titulo}
        </Button>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  cardcontainer: {
    paddingBottom: height * 0.01,
    width: "100%",
  },
  footer: {
    position: "absolute",
    height: height * 0.25,
    left: 0,
    top: height - 40,
    width: width * 0.5,
    alignContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
});

export default memo(InicioScreen);
