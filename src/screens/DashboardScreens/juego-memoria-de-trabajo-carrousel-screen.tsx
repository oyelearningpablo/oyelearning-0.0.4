import React, { memo } from "react";
import { StyleSheet } from "react-native";
import Background from "../../components/Background";
import Header from "../../components/Header";
import Paragraph from "../../components/Paragraph";
import CustomCarousel from "../../components/custom-carousel";
import { height, width } from "../../core/utils";
import { Navigation } from "../../types";
//import { preguntasEvaluacion } from "../../assets/data/preguntas-evaluacion";
import { FAB } from "react-native-paper";
import { theme } from "../../core/theme";

import { useSelector } from "react-redux";

interface ItemProps {
  title: string;
  text: string;
  pregunta: string;
  info: string;
  respuestasNumber?: Array<number>;
  respuestasString?: Array<string>;
}

type Props = {
  navigation: Navigation;
};

function obtenerFuncionCognitiva(preguntasEvaluacion: any[]): string {
  // TODO para debug puesto a 1 para que retorne el lunes
  const diaDeLaSemana = 1;
  //const diaDeLaSemana = hoy.getDay();
  const resultado = preguntasEvaluacion.find(
    (elemento) => elemento.numeroDiaSemana === diaDeLaSemana
  );

  return resultado?.funcionCognitiva || "";
}

function obtenerPreguntasEvaluacionSegunDiaDeLaSemana(
  preguntasEvaluacion: any[]
): ItemProps[] {
  const hoy = new Date();

  // TODO para debug puesto a 1 para que retorne el lunes
  const diaDeLaSemana = 1;
  //const diaDeLaSemana = hoy.getDay();
  const resultado = preguntasEvaluacion.find(
    (elemento: any) => elemento.numeroDiaSemana === diaDeLaSemana
  );
  const indicadores =
    resultado?.indicadores.map((element: any) => {
      const { pregunta, info, respuestas } = element;
      let esArrayDeStrings = false;
      let esArrayDeNumbers = false;
      respuestas.forEach((element: any) => {
        if (typeof element === "string") {
          esArrayDeStrings = true;
        } else {
          esArrayDeStrings = false;
        }
      });
      respuestas.forEach((element: any) => {
        if (typeof element === "number") {
          esArrayDeNumbers = true;
        } else {
          esArrayDeNumbers = false;
        }
      });
      if (esArrayDeStrings)
        return {
          title: "",
          text: "",
          pregunta,
          info,
          respuestasString: respuestas.slice(),
          respuestasNumber: [],
        };
      else if (esArrayDeNumbers) {
        return {
          title: "",
          text: "",
          pregunta,
          info,
          respuestasString: [],
          respuestasNumber: respuestas.slice(),
        };
      } else {
        return {
          title: "",
          text: "",
          pregunta,
          info,
          respuestasNumber: [],
          respuestasString: [],
        };
      }
    }) || [];
  return indicadores;
}

const textoProcesoEvaluacion =
  "Para realizar la evaluación, presiona sobre las estrellas";

const guardarEvaluacion = async (evaluacion: number[]) => {
  try {
    /*
    const user = await Auth.currentAuthenticatedUser();
    const response = await API.graphql(
      graphqlOperation(createEvaluacion, {
        input: {
          dimension: "Prueba Guardado",
          sesion: 15,
          evaluaciones: [...evaluacion],
          userId: user.attributes.sub,
          userName: user.username,
        },
      })
    );
    */
    console.log("Response>>>>>>>>>>>>> :\n");
    //console.log(response);
  } catch (e) {
    console.log("ERROR>>>>>>>>>", e);
  }
};

const JuegoMemoriaDeTrabajoCarrouselScreen = ({ navigation }: Props) => {
  const preguntasEvaluacion = useSelector(
    (state: any) => state.dimensionComunicativa.sesion1.preguntasEvaluacion
  );
  const evaluacion = useSelector((state: any) => state.evaluacion);
  return (
    <Background>
      <Header>{obtenerFuncionCognitiva(preguntasEvaluacion)}</Header>
      <Paragraph>{textoProcesoEvaluacion}</Paragraph>
      <CustomCarousel
        preguntasEvaluacion={obtenerPreguntasEvaluacionSegunDiaDeLaSemana(
          preguntasEvaluacion
        )}
      />
      <FAB
        style={styles.fab}
        small
        icon="home"
        onPress={() => {
          guardarEvaluacion(evaluacion);
          navigation.navigate("InicioScreen");
        }}
        label="Volver a inicio"
      />
    </Background>
  );
};

const styles = StyleSheet.create({
  videocontainer: {
    paddingBottom: height * 0.01,
  },
  cardcontainer: {
    paddingBottom: height * 0.01,
  },
  footer: {
    position: "absolute",
    height: height * 0.25,
    //left: 0,
    top: height - 40,
    width: width * 0.5,
    alignContent: "flex-end",
    alignItems: "flex-end",
    alignSelf: "flex-end",
    marginLeft: 1,
  },
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
    zIndex: 1,
    backgroundColor: theme.colors.primary,
  },
});

export default memo(JuegoMemoriaDeTrabajoCarrouselScreen);
