import React, { memo, useEffect, useState } from "react";
import { StyleSheet } from "react-native";
import { FAB } from "react-native-paper";
import Background from "../../components/Background";
import Header from "../../components/Header";
import { Loading } from "../../components/Loading";
import YoutubeVideo from "../../components/youtube-video";
import { height } from "../../core/utils";
import { Navigation } from "../../types";

import { useSelector } from "react-redux";
import { theme } from "../../core/theme";

type Props = {
  navigation: Navigation;
};

type PuntosDeAtencion = {
  texto: string;
  segundo: number;
};

const JuegoMemoriaDeTrabajoInicioScreen = ({ navigation }: Props) => {
  const [loading, setLoading] = useState(true);
  const [listaPuntosAtencion, setListaPuntosAtencion] = useState<Array<any>>(
    []
  );
  const dimensionComunicativaSesion1 = useSelector(
    (state: any) => state.dimensionComunicativa.sesion1
  );

  useEffect(() => {
    setListaPuntosAtencion(dimensionComunicativaSesion1.listaPuntosAtencion);
    setLoading(false);
  }, []);

  return (
    <Background>
      <Header>{dimensionComunicativaSesion1.header}</Header>
      {!loading ? (
        <YoutubeVideo
          videoId="0r01jne0IEY"
          seekFunction={true}
          seekParameters={listaPuntosAtencion}
        />
      ) : (
        <Loading />
      )}
      <FAB
        style={styles.fab}
        small
        icon="rocket"
        onPress={() =>
          navigation.navigate("JuegoMemoriaDeTrabajoCarrouselScreen")
        }
        label="Evaluar"
      />
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  videocontainer: {
    paddingBottom: height * 0.01,
  },
  cardcontainer: {
    paddingBottom: height * 0.01,
  },
  footer: {
    paddingTop: height * 0.01,
  },
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
    backgroundColor: theme.colors.primary,
  },
});

export default memo(JuegoMemoriaDeTrabajoInicioScreen);
