import React, { memo, useEffect, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { useSelector } from "react-redux";
import Background from "../../components/Background";
import Header from "../../components/Header";
import RNPCard from "../../components/RNPCard";
import YoutubeVideo from "../../components/youtube-video";
import { height } from "../../core/utils";
import { Navigation } from "../../types";

type Props = {
  navigation: Navigation;
};

type Response = {
  textosExplicacionBreve: Array<Tarjeta>;
};

type Tarjeta = {
  categoria: string;
  texto: string;
};

const ExplicacionBreveScreen = ({ navigation }: Props) => {
  const [loading, setLoading] = useState(true);
  const [titulo, setTitulo] = useState<Array<string>>([]);
  const [subTitulos, setSubTitulos] = useState<Array<string>>([]);
  const explicacionBreveScreen = useSelector(
    (state: any) => state.explicacionBreveScreen
  );

  const videoLoading = useSelector((state: any) => state.videoLoading);

  useEffect(() => {
    setTitulo([explicacionBreveScreen.texto1]);
    setSubTitulos([
      explicacionBreveScreen.item1,
      explicacionBreveScreen.item2,
      explicacionBreveScreen.item3,
      explicacionBreveScreen.item4,
    ]);
    setLoading(false);
  }, []);

  return (
    <Background>
      <Header>{explicacionBreveScreen.header}</Header>

      <ScrollView>
        <View style={styles.videocontainer}>
          <YoutubeVideo videoId="d2Fud46xFPQ" />
        </View>
        <View style={styles.cardcontainer}>
          <RNPCard subtitulos={titulo} />
        </View>
        <View style={styles.cardcontainer}>
          <RNPCard
            titulo={""}
            subtitulos={subTitulos}
            navegacion={() => navigation.navigate("MenuDimensionalScreen")}
            tituloNavegacion="Entendido"
          />
        </View>
      </ScrollView>
    </Background>
  );
};

const styles = StyleSheet.create({
  videocontainer: {
    paddingBottom: height * 0.01,
  },
  cardcontainer: {
    paddingBottom: height * 0.01,
  },
});

export default memo(ExplicacionBreveScreen);
