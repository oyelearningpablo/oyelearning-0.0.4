import React, { memo, useEffect, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { useSelector } from "react-redux";
import Background from "../../components/Background";
import Header from "../../components/Header";
import { Loading } from "../../components/Loading";
import RNPCard from "../../components/RNPCard";
import YoutubeVideo from "../../components/youtube-video";
import { height } from "../../core/utils";
import { Navigation } from "../../types";

type Props = {
  navigation: Navigation;
};

type Tarjeta = {
  categoria: string;
  texto: string;
};

const DimensionComunicativaScreen = ({ navigation }: Props) => {
  const [loading, setLoading] = useState(true);
  const [titulo, setTitulo] = useState<string>("");
  const [subTitulos, setSubTitulos] = useState<Array<string>>([]);
  const dimensionComunicativa = useSelector(
    (state: any) => state.dimensionComunicativa
  );

  useEffect(() => {
    setTitulo(dimensionComunicativa.rpnCard1.texto1);
    setSubTitulos([
      dimensionComunicativa.rpnCard1.item1,
      dimensionComunicativa.rpnCard1.item2,
      dimensionComunicativa.rpnCard1.item3,
      dimensionComunicativa.rpnCard1.item4,
    ]);
    setLoading(false);
  }, []);
  return (
    <Background>
      <Header>Dimensión Comunicativa</Header>
      {!loading ? (
        <ScrollView>
          <View style={styles.videocontainer}>
            <YoutubeVideo videoId="lAfCWiKD3LU" />
          </View>
          <View style={styles.cardcontainer}>
            <RNPCard
              titulo={titulo}
              subtitulos={subTitulos}
              navegacion={() =>
                navigation.navigate("JuegoMemoriaDeTrabajoInicioScreen")
              }
              tituloNavegacion="Entendido"
            />
          </View>
        </ScrollView>
      ) : (
        <Loading />
      )}
    </Background>
  );
};

const styles = StyleSheet.create({
  videocontainer: {
    paddingBottom: height * 0.01,
  },
  cardcontainer: {
    paddingBottom: height * 0.01,
  },
});

export default memo(DimensionComunicativaScreen);
