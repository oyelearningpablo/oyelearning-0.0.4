import React, { memo, useEffect, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import { Divider } from "react-native-paper";
import { useSelector } from "react-redux";
import Background from "../../components/Background";
import Header from "../../components/Header";
import { Loading } from "../../components/Loading";
import RNPCard from "../../components/RNPCard";
import { height } from "../../core/utils";
import { Navigation } from "../../types";

type CardProps = {
  title: string;
  icon: string;
  ruta: string;
};

const localData = [
  {
    title: "Dimensión Comunicativa",
    icon: "dimension-comunicativa",
    ruta: "DimensionComunicativaScreen",
  },
  {
    title: "Dimensión Naturalista",
    icon: "dimension-naturalista",
    ruta: null,
  },
  {
    title: "Dimensión Matemática",
    icon: "dimension-matematica",
    ruta: null,
  },
  {
    title: "Dimensión Visio-Espacial",
    icon: "dimension-visioespacial",
    ruta: null,
  },
  {
    title: "Dimensión Artístico-Musical",
    icon: "artistico-musical",
    ruta: null,
  },
  {
    title: "Dimensión Cinestésica",
    icon: "dimension-cinestesica",
    ruta: null,
  },
  {
    title: "Dimensión Emocional",
    icon: "dimension-emocional",
    ruta: null,
  },
];

type Props = {
  navigation: Navigation;
};

const MenuDimensionalScreen = ({ navigation }: Props) => {
  const [loading, setLoading] = useState(true);
  const [lista, setLista] = useState<Array<any>>([]);
  const menuDimensionalScreen = useSelector(
    (state: any) => state.menuDimensionalScreen
  );
  useEffect(() => {
    setLista(menuDimensionalScreen.lista);
    setLoading(false);
  }, []);

  return (
    <Background>
      <Header>{menuDimensionalScreen.header}</Header>
      {!loading ? (
        <ScrollView>
          {lista.map((element, index) => {
            const { title, icon, ruta } = element;
            return (
              <View key={`view${index}`} style={styles.cardcontainer}>
                {ruta ? (
                  <RNPCard
                    titulo={title}
                    recursoImagen={icon}
                    navegacion={() => navigation.navigate(ruta)}
                    tituloNavegacion="Comenzar"
                  />
                ) : (
                  <RNPCard
                    titulo={title}
                    recursoImagen={icon}
                    tituloNavegacion="Proximamente"
                  />
                )}

                <Divider key={`div${index}`} />
              </View>
            );
          })}
        </ScrollView>
      ) : (
        <Loading />
      )}
    </Background>
  );
};

const styles = StyleSheet.create({
  cardcontainer: {
    paddingBottom: height * 0.01,
  },
});

export default memo(MenuDimensionalScreen);
