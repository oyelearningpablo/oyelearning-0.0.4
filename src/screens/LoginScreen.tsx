import React, { memo, useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import BackButton from "../components/BackButton";
import Background from "../components/Background";
import Button from "../components/Button";
import Header from "../components/Header";
import Logo from "../components/Logo";
import TextInput from "../components/TextInput";
import { theme } from "../core/theme";
import { Navigation } from "../types";
/*
async function signIn({ username, password }) {
  const response = await Auth.signIn(username, password);
  return response;
}

async function getCurrentAuthenticatedUser() {
  const response = await Auth.currentAuthenticatedUser({
    bypassCache: false, // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
  });
  return response;
}
*/
type Props = {
  navigation: Navigation;
};

const LoginScreen = ({ navigation }: Props) => {
  const [name, setName] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });
  const [signInError, setSignInError] = useState("");
  /*
  useEffect(() => {
    getCurrentAuthenticatedUser()
      .then((user) => {
        console.log("getCurrentAuthenticatedUser", user);
        const { username } = user;
        console.log("getCurrentAuthenticatedUser username", username);
        setName({ value: username, error: "" });
      })
      .catch((err) => console.log("getCurrentAuthenticatedUser error", err));
  }, []);
  */
  /*
  const _onLoginPressed = () => {
    const nameError = nameValidator(name.value);
    const passwordError = passwordValidator(password.value);

    if (nameError || passwordError) {
      setName({ ...name, error: nameError });
      setPassword({ ...password, error: passwordError });
      return;
    }
    const userName = name.value;
    const passWord = password.value;
    signIn({ username: userName, password: passWord })
      .then((response) => {
        console.log("usuario correcto, padentro", response);
        navigation.navigate("InicioScreen");
      })
      .catch((error) => {
        console.log("error signing in", error);
        setSignInError(error);
      });
  };
  */
  return (
    <Background>
      <View style={styles.container}>
        <BackButton goBack={() => navigation.navigate("HomeScreen")} />

        <Logo />

        <Header>Bienvenido otra vez.</Header>

        <TextInput
          label="Nombre"
          returnKeyType="next"
          value={name.value}
          onChangeText={(text) => setName({ value: text, error: "" })}
          error={!!name.error}
          errorText={name.error}
        />

        <TextInput
          label="Password"
          returnKeyType="done"
          value={password.value}
          onChangeText={(text) => setPassword({ value: text, error: "" })}
          error={!!password.error}
          errorText={password.error}
          secureTextEntry
        />

        <View style={styles.forgotPassword}>
          <TouchableOpacity
            onPress={() => navigation.navigate("ForgotPasswordScreen")}
          >
            <Text style={styles.label}>Olvidaste tu password?</Text>
          </TouchableOpacity>
        </View>

        <Button
          mode="contained"
          onPress={() => navigation.navigate("InicioScreen")}
        >
          Login
        </Button>
        {signInError ? (
          <Text style={styles.label}>{JSON.stringify(signInError)}</Text>
        ) : null}

        <View style={styles.row}>
          <Text style={styles.label}>Todavía no tienes cuenta? </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate("RegisterScreen")}
          >
            <Text style={styles.link}>Alta</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  forgotPassword: {
    width: "100%",
    alignItems: "flex-end",
    marginBottom: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  label: {
    color: theme.colors.secondary,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
});

export default memo(LoginScreen);
