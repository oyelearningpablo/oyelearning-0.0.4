import React, { memo } from "react";
import { StyleSheet, View } from "react-native";

import Background from "../components/Background";
import Button from "../components/Button";
import Header from "../components/Header";
import Logo from "../components/Logo";
import Paragraph from "../components/Paragraph";
import { Navigation } from "../types";

type Props = {
  navigation: Navigation;
};

const HomeScreen = ({ navigation }: Props) => {
  return (
    <Background>
      <View style={styles.container}>
        <Logo />
        <Header>OyeLearning</Header>

        <Paragraph>
          Si eres un nuevo usuario date de alta en la aplicación.
        </Paragraph>
        <Button
          mode="contained"
          onPress={() => navigation.navigate("LoginScreen")}
        >
          Ya tengo cuenta
        </Button>
        <Button
          mode="outlined"
          onPress={() => navigation.navigate("RegisterScreen")}
        >
          Soy nuevo
        </Button>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default memo(HomeScreen);
