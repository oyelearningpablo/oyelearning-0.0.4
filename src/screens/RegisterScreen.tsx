import React, { memo, useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import BackButton from "../components/BackButton";
import Background from "../components/Background";
import Button from "../components/Button";
import Header from "../components/Header";
import Logo from "../components/Logo";
import TextInput from "../components/TextInput";
import { theme } from "../core/theme";
import { Navigation } from "../types";
/*
async function signUp({ username, password, email }) {
  const response = await Auth.signUp({
    username,
    password,
    attributes: {
      email,
    },
  });
  return response;
}

async function confirmSignUp({ username, code }) {
  const response = await Auth.confirmSignUp(username, code);
  return response;
}
*/
type Props = {
  navigation: Navigation;
};

const RegisterScreen = ({ navigation }: Props) => {
  const [name, setName] = useState({ value: "", error: "" });
  const [email, setEmail] = useState({ value: "", error: "" });
  const [password, setPassword] = useState({ value: "", error: "" });
  const [signUpError, setSignUpError] = useState("");
  const [codigo, setCodigo] = useState({ value: "" });
  const [confirmSignUpError, setConfirmSignUpError] = useState("");
  const [showConfirmEmail, setShowConfirmEmail] = useState(false);
  /*
  const _onSignUpPressed = () => {
    const nameError = nameValidator(name.value);
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);

    if (emailError || passwordError || nameError) {
      setName({ ...name, error: nameError });
      setEmail({ ...email, error: emailError });
      setPassword({ ...password, error: passwordError });
      return;
    }

    const userName = name.value;
    const passWord = password.value;
    const eMail = email.value;
    signUp({ username: userName, password: passWord, email: eMail })
      .then((response) => {
        console.log("signUp correcto", response);
        setShowConfirmEmail(true);
      })
      .catch((error) => {
        console.log("error signUp", error);
        setSignUpError(error);
      });
  };

  const _onConfirmPressed = () => {
    const userName = name.value;
    const codigoValue = codigo.value;

    confirmSignUp({ username: userName, code: codigoValue })
      .then((response) => {
        console.log("confirmSignUp correcto", response);
        navigation.navigate("LoginScreen");
      })
      .catch((error) => {
        console.log("error confirmSignUp", error);
        setConfirmSignUpError(error);
      });
  };
  */
  return (
    <Background>
      <View style={styles.container}>
        <BackButton goBack={() => navigation.navigate("HomeScreen")} />

        <Logo />

        <Header>Crear cuenta</Header>

        <TextInput
          label="Nombre"
          returnKeyType="next"
          value={name.value}
          onChangeText={(text) => setName({ value: text, error: "" })}
          error={!!name.error}
          errorText={name.error}
        />
        {!showConfirmEmail ? (
          <>
            <TextInput
              label="Email"
              returnKeyType="next"
              value={email.value}
              onChangeText={(text) => setEmail({ value: text, error: "" })}
              error={!!email.error}
              errorText={email.error}
              autoCapitalize="none"
              autoCompleteType="email"
              textContentType="emailAddress"
              keyboardType="email-address"
            />

            <TextInput
              label="Password"
              returnKeyType="done"
              value={password.value}
              onChangeText={(text) => setPassword({ value: text, error: "" })}
              error={!!password.error}
              errorText={password.error}
              secureTextEntry
            />

            <Button mode="contained" style={styles.button}>
              Alta
            </Button>
            {signUpError ? (
              <Text style={styles.label}>{JSON.stringify(signUpError)}</Text>
            ) : null}
          </>
        ) : (
          <>
            <TextInput
              label="Codigo"
              returnKeyType="next"
              value={codigo.value}
              onChangeText={(text) => setCodigo({ value: text })}
            />
            <Button mode="contained" style={styles.button}>
              Confirmar codigo
            </Button>
            {confirmSignUpError ? (
              <Text style={styles.label}>
                {JSON.stringify(confirmSignUpError)}
              </Text>
            ) : null}
          </>
        )}
        <View style={styles.row}>
          <Text style={styles.label}>Ya tienes cuenta en OyeLearning? </Text>
          <TouchableOpacity onPress={() => navigation.navigate("LoginScreen")}>
            <Text style={styles.link}>Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Background>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  label: {
    color: theme.colors.secondary,
  },
  button: {
    marginTop: 24,
  },
  row: {
    flexDirection: "row",
    marginTop: 4,
  },
  link: {
    fontWeight: "bold",
    color: theme.colors.primary,
  },
});

export default memo(RegisterScreen);
