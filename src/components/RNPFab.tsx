import * as React from "react";
import { FAB, Portal, Provider } from "react-native-paper";

type Props = {
  navegacionHome: () => void;
};

const RNPFab = ({ navegacionHome }: Props) => {
  const [state, setState] = React.useState({ open: false });

  const onStateChange = (): any => setState({ open: !open });

  const { open } = state;

  return (
    <Provider>
      <Portal>
        <FAB.Group
          open={open}
          icon={open ? "minus" : "plus"}
          visible={true}
          actions={[
            {
              icon: "home",
              label: "Inicio",
              onPress: navegacionHome,
            },
            {
              icon: "email",
              label: "Contacta con nosotros",
              onPress: () => console.log("Pressed email"),
            },
            {
              icon: "face",
              label: "Perfil",
              onPress: () => console.log("Pressed notifications"),
              small: false,
            },
          ]}
          onStateChange={onStateChange}
          onPress={() => {
            if (open) {
              // do something if the speed dial is open
            }
          }}
        />
      </Portal>
    </Provider>
  );
};

export default RNPFab;
