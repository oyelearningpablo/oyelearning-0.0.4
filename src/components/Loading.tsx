import React from "react";
import { View } from "react-native";
import { height, width } from "../core/utils";
import { Animacion } from "./animacion";
export const Loading = function Loading() {
  return (
    <View style={{ height, width, justifyContent: "center" }}>
      <Animacion recurso="bolitas" />
    </View>
  );
};
