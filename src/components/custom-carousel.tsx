import React, { memo, useState } from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import { AirbnbRating } from "react-native-ratings";
import { theme } from "../core/theme";
import { height, width } from "../core/utils";
import { AnimacionIndex } from "./animacion";
import Header from "./Header";
import Modal from "./Modal";
import Paragraph from "./Paragraph";
//import { preguntasEvaluacion } from "../assets/data/preguntas-evaluacion";
//import Carousel from "react-native-snap-carousel";
import { Dimensions, View } from "react-native";
import Carousel from "react-native-reanimated-carousel";
import { useDispatch } from "react-redux";
import { updateEvaluacion } from "../store/userAction";

interface ItemProps {
  title: string;
  text: string;
  pregunta: string;
  info: string;
  respuestasNumber?: Array<number>;
  respuestasString?: Array<string>;
}

type Props = {
  children?: React.ReactNode;
  preguntasEvaluacion: any;
};

type State = {
  activeIndex: number;
  carouselItems: ItemProps[];
};
const CustomCarousel = ({ preguntasEvaluacion }: Props) => {
  const width = Dimensions.get("window").width;
  const [activeIndex, setActiveIndex] = useState(0);
  const [carouselItems, setCarouselItems] = useState(preguntasEvaluacion);
  const [ref, setRef] = useState(React.createRef<any>());

  const dispatch = useDispatch();
  const submitEvaluacion = (value: number, index: number) =>
    dispatch(updateEvaluacion(value, index));

  const renderItem = ({ item, index }: { item: ItemProps; index: number }) => {
    const { info, respuestasString } = item;
    const airbnbRatingCount = respuestasString?.length;

    return (
      <View
        style={{
          backgroundColor: theme.colors.surface,
          borderRadius: 5,
          //height: height * 0.55,
          padding: 50,
          marginRight: width * 0.1,
        }}
      >
        <Header>
          {"Evaluación #"}
          {index + 1}
          {"/"}
          {carouselItems.length}
        </Header>
        <Paragraph>
          {item.pregunta} <Modal texto={info} />
        </Paragraph>
        <View
          style={{
            flexDirection: "row",
          }}
        >
          <View style={{ flex: 3, alignItems: "flex-start" }}>
            <AirbnbRating
              count={airbnbRatingCount}
              reviews={respuestasString}
              defaultRating={airbnbRatingCount}
              size={40}
              onFinishRating={(value) => {
                if (index < carouselItems.length) {
                  setActiveIndex(index + 1);
                } else {
                  null;
                }
                submitEvaluacion(value, index);
              }}
            />
          </View>
          <View style={{ flex: 1, alignItems: "flex-end" }}>
            <AnimacionIndex index={index} />
          </View>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View>
        <Carousel
          ref={ref}
          data={carouselItems}
          width={width}
          height={width}
          renderItem={renderItem}
          onSnapToItem={(index: number) => setActiveIndex(index)}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width,
  },
  container: {
    flex: 1,
    padding: 20,
    width,
    maxWidth: width * 0.95,
    alignSelf: "center",
    height,
    maxHeight: height * 0.99,
    //alignItems: 'center',
    //justifyContent: 'center',
  },
});

export default memo(CustomCarousel);
