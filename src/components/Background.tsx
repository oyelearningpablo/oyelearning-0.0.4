import { LinearGradient } from "expo-linear-gradient";
import React, { memo } from "react";
import { KeyboardAvoidingView, StyleSheet } from "react-native";

type Props = {
  children: React.ReactNode;
};

const Background = ({ children }: Props) => (
  <LinearGradient
    colors={["#ffffff", "#ffffff", "#a5bdd9"]} // Use the three colors from the color scheme
    style={styles.background}
  >
    <KeyboardAvoidingView style={styles.container} behavior="padding">
      {children}
    </KeyboardAvoidingView>
  </LinearGradient>
);

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: "100%",
  },
  container: {
    flex: 1,
    padding: 20,
    width: "100%",
    //maxWidth: 340,
    //alignSelf: 'center',
    //alignItems: 'center',
    //justifyContent: 'center',
  },
});

export default memo(Background);
