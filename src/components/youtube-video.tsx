import { LinearGradient } from "expo-linear-gradient";
import React, { memo, useCallback, useEffect, useRef, useState } from "react";
import {
  ScrollView,
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native";
import { Divider } from "react-native-paper";
import YoutubePlayer, { YoutubeIframeRef } from "react-native-youtube-iframe";
import Paragraph from "./Paragraph";

import { theme } from "../core/theme";
import { height, width } from "../core/utils";

import { useDispatch } from "react-redux";
import { setVideoLoading } from "../store/userAction";
import Modal from "./Modal";

import { useSelector } from "react-redux";

export interface YoutubeVideoProps {
  /**
   * An optional style override useful for padding & margin.
   */
  videoId: string;
  seekFunction?: boolean;
  seekParameters?: Array<any>;
  style?: StyleProp<ViewStyle>;
}

/**
 * Describe your component here
 *
 */
const YoutubeVideo = (props: YoutubeVideoProps) => {
  const { seekFunction, seekParameters } = props;
  const playerRef = useRef<YoutubeIframeRef>(null);
  const [playing, setPlaying] = useState(false);
  const videoHeight = (width / 16) * 9;
  const dispatch = useDispatch();
  const videoLoading = useSelector((state: any) => state.videoLoading);
  useEffect(() => {
    console.error("setVideoLoading(true)");
    dispatch(setVideoLoading(true));
  }, []);

  const onStateChange = useCallback((state) => {
    if (state === "ended") {
      setPlaying(false);
    }
  }, []);

  const onReady = useCallback(() => {
    console.error("setVideoLoading(false)");
    dispatch(setVideoLoading(false));
  }, []);

  const onError = useCallback((error) => {
    console.log("onError....", error);
    dispatch(setVideoLoading(false));
  }, []);

  const renderVideoPlayer = () => (
    <View>
      <View>
        <YoutubePlayer
          ref={playerRef}
          height={videoHeight}
          play={playing}
          videoId={props.videoId}
          onChangeState={onStateChange}
          onReady={() => onReady()}
          onError={(error) => onError(error)}
        />
      </View>
      {seekFunction ? (
        <View>
          <Paragraph>
            Puntos de atención{" "}
            <Modal texto="Pulsa sobre la lista para viajar a distintos puntos del video" />
          </Paragraph>
        </View>
      ) : null}
      {seekFunction ? (
        <ScrollView style={styles.scrollView}>
          {seekParameters?.map((element, index) => {
            const { texto, segundo } = element;
            return (
              <View key={`view${index}`} style={styles.cardcontainer}>
                <View
                  style={{
                    flexDirection: "row",

                    height: height * 0.1,
                    justifyContent: "center",
                    alignItems: "center",
                    padding: 3,
                  }}
                >
                  <TouchableOpacity
                    onPress={() => playerRef.current?.seekTo(segundo, true)}
                  >
                    <Text style={styles.label}>
                      #{index + 1}
                      {"/"}
                      {seekParameters.length}
                      {":\t"} {texto}{" "}
                    </Text>
                  </TouchableOpacity>
                </View>
                <Divider key={`div${index}`} />
              </View>
            );
          })}
        </ScrollView>
      ) : null}
    </View>
  );

  return (
    <View
      style={{
        zIndex: 1,
      }}
    >
      <LinearGradient colors={["#d1dde8", "#ffffff"]}>
        {renderVideoPlayer()}
      </LinearGradient>
    </View>
  );
};

const styles = StyleSheet.create({
  cardcontainer: {
    paddingBottom: height * 0.01,
  },
  label: {
    color: theme.colors.secondary,
  },
  scrollView: {
    height: "45%",
    borderWidth: 0.5,
    borderRadius: 1,
    borderColor: theme.colors.primary,
    padding: 3,
  },
  contentContainer: {
    justifyContent: "center",
    alignItems: "center",
    //backgroundColor: 'lightgrey',
    //paddingBottom: 10,
  },
});

export default memo(YoutubeVideo);
/* BOTON Y METODO EN DESUSO PARA REPRODUCCION DEL VIDEO
const togglePlaying = useCallback(() => {
    setPlaying((prev) => !prev);
  }, []);

<Card>
        <ReactNativeElementsButton
          icon={<Icon name="arrow-right" size={15} color="white" />}
          title={playing ? "pause" : "play"}
          onPress={togglePlaying}
        />
      </Card>

*/
