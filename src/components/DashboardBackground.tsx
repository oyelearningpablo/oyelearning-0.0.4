import { LinearGradient } from "expo-linear-gradient";
import React, { memo } from "react";
import { KeyboardAvoidingView, StyleSheet } from "react-native";

import { height, width } from "../core/utils";

type Props = {
  children: React.ReactNode;
};

const DashboardBackground = ({ children }: Props) => (
  <LinearGradient
    colors={["#8de5ea", "#5eb5d3", "#3d8ebf"]}
    style={styles.background}
  >
    <KeyboardAvoidingView style={styles.container} behavior="padding">
      {children}
    </KeyboardAvoidingView>
  </LinearGradient>
);

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width,
  },
  container: {
    flex: 1,
    padding: 20,
    width,
    maxWidth: width * 0.95,
    alignSelf: "center",
    height,
    maxHeight: height * 0.99,
  },
});

export default memo(DashboardBackground);
