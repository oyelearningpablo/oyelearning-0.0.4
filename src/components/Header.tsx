import React, { memo } from "react";
import { StyleSheet, Text, View } from "react-native";
import { theme } from "../core/theme";

type Props = {
  children: React.ReactNode;
};

const Header = ({ children }: Props) => (
  <View style={styles.container}>
    <Text style={styles.header}>{children}</Text>
    <View
      style={{
        borderBottomColor: "black",
        borderBottomWidth: 1,
      }}
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    paddingBottom: 5,
  },
  header: {
    fontSize: 26,
    color: theme.colors.secondary,
    fontWeight: "bold",
    paddingVertical: 14,
    alignItems: "center",
    alignSelf: "center",
  },
});

export default memo(Header);
