import LottieView from "lottie-react-native";
import React from "react";
import { StyleProp, ViewStyle } from "react-native";

export interface AnimacionProps {
  /**
   * An optional style override useful for padding & margin.
   */
  recurso: string;
  style?: StyleProp<ViewStyle>;
}

/**
 * Describe your component here
 */

export const Animacion = function Animacion(props: AnimacionProps) {
  const { recurso } = props;

  if (recurso === "sunset") {
    return (
      <LottieView
        source={require("../assets/lottie/sunset.json")}
        autoPlay
        loop
      />
    );
  } else if (recurso === "bolitas") {
    return (
      <LottieView
        source={require("../assets/lottie/bolitas.json")}
        autoPlay
        loop
      />
    );
  } else if (recurso === "gears") {
    return (
      <LottieView
        source={require("../assets/lottie/gears-animation.json")}
        autoPlay
        loop
      />
    );
  } else if (recurso === "brainbulb") {
    return (
      <LottieView
        source={require("../assets/lottie/brain-bulb-with-gears.json")}
        autoPlay
        loop
      />
    );
  } else if (recurso === "hombrecerebro") {
    return (
      <LottieView
        source={require("../assets/lottie/hombre-con-cerebro.json")}
        autoPlay
        loop
      />
    );
  } else if (recurso === "dynamicgears") {
    return (
      <LottieView
        source={require("../assets/lottie/dynamic-gear.json")}
        autoPlay
        loop
      />
    );
  } else if (recurso === "brainyquestion") {
    return (
      <LottieView
        source={require("../assets/lottie/brainy-questions.json")}
        autoPlay
        loop
      />
    );
  } else if (recurso === "funnybrain") {
    return (
      <LottieView
        source={require("../assets/lottie/funny-brain.json")}
        autoPlay
        loop
      />
    );
  } else if (recurso === "braincircuit") {
    return (
      <LottieView
        source={require("../assets/lottie/brain-circuit.json")}
        autoPlay
        loop
      />
    );
  } else {
    return (
      <LottieView
        source={require("../assets/lottie/funny-brain.json")}
        autoPlay
        loop
      />
    );
  }
};

const recursosDisponibles = [
  "bolitas",
  "brainbulb",
  "hombrecerebro",
  "brainyquestion",
  "funnybrain",
  "braincircuit",
];

export const AnimacionRandom = function AnimacionRandom() {
  return (
    <Animacion
      recurso={
        recursosDisponibles[
          Math.floor(Math.random() * recursosDisponibles.length)
        ]
      }
    />
  );
};

export interface AnimacionIndexProps {
  index: number;
}

export const AnimacionIndex = function AnimacionIndex(
  props: AnimacionIndexProps
) {
  const { index } = props;
  return <Animacion recurso={recursosDisponibles[index]} />;
};
