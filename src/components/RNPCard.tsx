import * as React from "react";
import { StyleSheet } from "react-native";
import { Button, Card, Paragraph, Title } from "react-native-paper";
import { theme } from "../core/theme";
import { FadeInView } from "./RNAnimated";

const CardCover = function CardCover(props: Props) {
  const { recursoImagen } = props;

  if (recursoImagen === "libre") {
    return <Card.Cover source={require("../assets/libre_cerebro.png")} />;
  } else if (recursoImagen === "dimension-comunicativa") {
    return (
      <Card.Cover source={require("../assets/dimension-comunicativa.png")} />
    );
  } else if (recursoImagen === "dimension-naturalista") {
    return (
      <Card.Cover source={require("../assets/dimension-naturalista.png")} />
    );
    //
  } else if (recursoImagen === "dimension-matematica") {
    return (
      <Card.Cover source={require("../assets/dimension-matematica.png")} />
    );
  } else if (recursoImagen === "dimension-visioespacial") {
    return (
      <Card.Cover source={require("../assets/dimension-visoespacial.png")} />
    );
  } else if (recursoImagen === "artistico-musical") {
    return <Card.Cover source={require("../assets/artistico-musical.png")} />;
  } else if (recursoImagen === "dimension-cinestesica") {
    return (
      <Card.Cover source={require("../assets/dimension-cinestesica.png")} />
    );
  } else if (recursoImagen === "dimension-emocional") {
    return <Card.Cover source={require("../assets/dimension-emocional.png")} />;
  } else {
    return <Card.Cover source={require("../assets/cactus.png")} />;
  }
};

type Props = {
  titulo?: string;
  subtitulos?: Array<string>;
  uriImagen?: string; //"https://picsum.photos/700"
  recursoImagen?: string;
  navegacion?: () => void;
  tituloNavegacion?: string;
};
const RNPCard = ({
  titulo,
  subtitulos,
  uriImagen,
  recursoImagen,
  navegacion,
  tituloNavegacion,
}: Props) => (
  <Card style={styles.card}>
    {uriImagen ? <Card.Cover source={{ uri: uriImagen }} /> : null}
    {recursoImagen ? <CardCover recursoImagen={recursoImagen} /> : null}
    <Card.Content>
      {titulo ? <Title style={styles.title}>{titulo} </Title> : null}
      {subtitulos?.map((subtitulo, index) => {
        return (
          <FadeInView key={index}>
            <Paragraph style={styles.subtitle}>{subtitulo}</Paragraph>
          </FadeInView>
        );
      })}
    </Card.Content>

    {tituloNavegacion ? (
      <Card.Actions style={{ justifyContent: "flex-end" }}>
        <Button onPress={navegacion} mode="contained">
          {tituloNavegacion}
        </Button>
      </Card.Actions>
    ) : null}
  </Card>
);

const styles = StyleSheet.create({
  card: {
    marginHorizontal: theme.spacing(2),
    marginVertical: theme.spacing(1),
    elevation: 3,
    backgroundColor: theme.colors.background,
    borderRadius: 8,
  },
  title: {
    fontFamily: theme.fonts.medium,
    fontSize: 20,
    color: theme.colors.primary,
  },
  subtitle: {
    fontFamily: theme.fonts.regular,
    fontSize: 16,
    color: "gray",
  },
  paragraph: {
    fontFamily: theme.fonts.regular,
    fontSize: 14,
    color: "black",
    marginTop: 8,
    marginBottom: 8,
    lineHeight: 20,
    paddingLeft: 16,
    paddingRight: 16,
  },
});
/*
const styles = StyleSheet.create({
  
});
*/
export default RNPCard;
