import { createAppContainer } from "react-navigation";
import {
  CardStyleInterpolators,
  createStackNavigator,
} from "react-navigation-stack";

import {
  ForgotPasswordScreen,
  HomeScreen,
  LoginScreen,
  RegisterScreen,
} from "./screens";

import DimensionComunicativaScreen from "./screens/DashboardScreens/dimension-comunicativa-screen";
import ExplicacionBreveScreen from "./screens/DashboardScreens/explicacion-breve-screen";
import InicioScreen from "./screens/DashboardScreens/inicio-screen";
import JuegoMemoriaDeTrabajoCarrouselScreen from "./screens/DashboardScreens/juego-memoria-de-trabajo-carrousel-screen";
import JuegoMemoriaDeTrabajoInicioScreen from "./screens/DashboardScreens/juego-memoria-de-trabajo-inicio-screen";
import MenuDimensionalScreen from "./screens/DashboardScreens/menu-dimensional-screen";

//https://github.com/react-navigation/stack/tree/master/example/src
//https://reactnavigation.org/docs/4.x/stack-navigator
const config = {
  animation: "spring",
  config: {
    stiffness: 1000,
    damping: 500,
    mass: 3,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};
const Router = createStackNavigator(
  {
    HomeScreen,
    LoginScreen,
    RegisterScreen,
    ForgotPasswordScreen,
    InicioScreen,
    ExplicacionBreveScreen,
    MenuDimensionalScreen,
    DimensionComunicativaScreen,
    JuegoMemoriaDeTrabajoInicioScreen,
    JuegoMemoriaDeTrabajoCarrouselScreen,
  },
  /*
  {
    initialRouteName: "HomeScreen",
    headerMode: "none",
    mode: "card",
    defaultNavigationOptions: {
      cardStyle: { backgroundColor: "transparent" },
      gestureEnabled: false,
      cardStyleInterpolator: ({ current: { progress } }) => {
        const opacity = progress.interpolate({
          inputRange: [0, 0.5, 0.9, 1],
          outputRange: [0, 0.25, 0.7, 1],
        });

        return {
          cardStyle: {
            opacity,
          },
        };
      },
    },
  }
  */
  {
    initialRouteName: "HomeScreen",
    headerMode: "none",
    defaultNavigationOptions: {
      cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid,
    },
  }
);
export default createAppContainer(Router);
//export default withAuthenticator(createAppContainer(Router));
/*
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  HomeScreen,
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
} from "./screens";

import InicioScreen from "./screens/DashboardScreens/inicio-screen";
import ExplicacionBreveScreen from "./screens/DashboardScreens/explicacion-breve-screen";
import MenuInteligenciasScreen from "./screens/DashboardScreens/menu-inteligencias-screen";
import InteligenciaLinguistica from "./screens/DashboardScreens/inteligencia-linguistica-screen";
import JuegoMemoriaDeTrabajoInicioScreen from "./screens/DashboardScreens/juego-memoria-de-trabajo-inicio-screen";
import JuegoMemoriaDeTrabajoCarrouselScreen from "./screens/DashboardScreens/juego-memoria-de-trabajo-carrousel-screen";

export default function Navigation() {
  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  );
}


const Stack = createNativeStackNavigator();

function RootNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        animation: "slide_from_right",
      }}
    >
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
      <Stack.Screen
        name="ForgotPasswordScreen,   "
        component={ForgotPasswordScreen}
      />
      <Stack.Screen name="InicioScreen" component={InicioScreen} />
      <Stack.Screen
        name="ExplicacionBreveScreen"
        component={ExplicacionBreveScreen}
      />
      <Stack.Screen
        name="MenuInteligenciasScreen"
        component={MenuInteligenciasScreen}
      />
      <Stack.Screen
        name="InteligenciaLinguistica"
        component={InteligenciaLinguistica}
      />
      <Stack.Screen
        name="JuegoMemoriaDeTrabajoInicioScreen"
        component={JuegoMemoriaDeTrabajoInicioScreen}
      />
      <Stack.Screen
        name="JuegoMemoriaDeTrabajoCarrouselScree"
        component={JuegoMemoriaDeTrabajoCarrouselScreen}
      />
    </Stack.Navigator>
  );
}
*/
