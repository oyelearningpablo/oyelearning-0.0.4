export const preguntasEvaluacion =[
    {   
        "dia": "lunes",
        "numeroDiaSemana": 1,
        "funcionCognitiva": "Evaluación Sesión #1",
        "indicadores":[
            {
                "pregunta": "Número de trabalenguas recordados sin ayuda",
                "info": "Número de trabalenguas que ha recitado sin ninguna ayuda. 0, 1, 2 o 3.",
                "respuestas": ["Mal", "Regular", "Excelente"]
            },
            {
                "pregunta": "Nivel de atención sostenida",
                "info": "Cómo le has visto de atento e interesado, en una escala de 1 a 5.",
                "respuestas": ["Mal", "Mediocre", "Regular", "Notable", "Excelente"] 
            },
            {
                "pregunta": "Reflexión metacognitiva lograda",
                "info": "Cómo valoras las reflexiones evocadas, en respuesta a las preguntas realizadas en la tarea, en una escala de 1 a 5.",
                "respuestas": ["Mal", "Mediocre", "Regular", "Notable", "Excelente"]
            },
            {
                "pregunta": "Calidad del tiempo compartido",
                "info": "¿Cómo evalúas la calidad del tiempo compartido? Ten en cuenta: satisfacción de ambos, aprendizaje del participante (es@ es tu hij@),  y formación del instructor (es@ eres tú).",
                "respuestas":["Mal", "Mediocre", "Regular", "Notable", "Excelente"]
            }
        ]
    }
];

/*

ANTERIOR, dependiente de los días. Revisar
export const preguntasEvaluacion =[
    {   
        "dia": "lunes",
        "numeroDiaSemana": 1,
        "funcionCognitiva": "Evaluacion Memoria de trabajo",
        "indicadores":[
            {
                "pregunta": "Numero de elementos recordados",
                "respuestas": [1,2,3,4]
            },
            {
                "pregunta": "Numero de veces que se repite la secuencia",
                "respuestas": [1,2,3,4,5] 
            },
            {
                "pregunta": "Numero de preseñalizaciones por elemento",
                "respuestas": [1,2,3,4] 
            },
            {
                "pregunta": "Nivel de interés",
                "respuestas": [1,2,3,4,5,6,7,8,9,10] 
            },
            {
                "pregunta": "Sostenimiento de la atención",
                "respuestas": [1,2,3,4,5,6,7,8,9,10] 
            },
            {
                "pregunta": "Recuerda el objetivo del juego",
                "respuestas": ["Si","No","En parte"] 
            }
        ]
    },
    {
        "dia": "martes",
        "numeroDiaSemana": 2,
        "funcionCognitiva": "Evaluacion Control inhibitorio",
        "indicadores":[
            {
                "pregunta": "Nivel de recuerdo del desarrollo del juego anterior",
                "respuestas": [1,2,3,4,5,6,7,8,9,10]
            },
            {
                "pregunta": "Numero de respuestas incongruentes o anticipaciones",
                "respuestas": [1,2,3,4,5,6,7,8,9,10]
            },
            {
                "pregunta": "Ha controlado sus impulsos?",
                "respuestas": ["Si","No","Con Ayuda"] 
            },
            {
                "pregunta": "Sostenimiento de la atención",
                "respuestas": [1,2,3,4,5,6,7,8,9,10] 
            },
            {
                "pregunta": "Dificultad del padre para entender y desarrollar los objetivos del juego",
                "respuestas": [1,2,3,4,5,6,7,8,9,10] 
            },
            {
                "pregunta": "Ha conseguido transferir lo aprendido a su experiencia (Metacognición)",
                "respuestas": ["Si","No","Con Ayuda"] 
            }
        ]
    },
    {
        "dia": "miercoles",
        "numeroDiaSemana": 3,
        "funcionCognitiva": "Evaluacion Flexibilidad cognitiva",
        "indicadores":[
            {
                "pregunta": "Nivel de recuerdo del desarrollo del juego anterior",
                "respuestas": [1,2,3,4,5,6,7,8,9,10]
            },
            {
                "pregunta": "Capacidad para flexibilizar las respuestas",
                "respuestas": [1,2,3,4,5,6,7,8,9,10]
            },
            {
                "pregunta": "Ha mostrado creatividad saliendose del guion preestablecido",
                "respuestas": ["Si","No","Con Ayuda"] 
            },
            {
                "pregunta": "Sostenimiento de la atención",
                "respuestas": [1,2,3,4,5,6,7,8,9,10] 
            },
            {
                "pregunta": "Dificultad del padre para entender y desarrollar los objetivos del juego",
                "respuestas": [1,2,3,4,5,6,7,8,9,10] 
            },
            {
                "pregunta": "Han conseguido padre/hijo crear sus propios resultados",
                "respuestas": ["Si","No","En parte"] 
            }
        ]
    },
    {
        "dia": "jueves",
        "numeroDiaSemana": 4,
        "funcionCognitiva": "Evaluacion Liderazgo cooperativo",
        "indicadores":[
            {
                "pregunta": "Se acordaba de la secuencia que ha propuesto",
                "respuestas": [1,2,3,4,5,6,7,8,9,10]
            },
            {
                "pregunta": "Sostenimiento de la atención",
                "respuestas": [1,2,3,4,5,6,7,8,9,10]
            },
            {
                "pregunta": "Ha sido capaz de dar pistas reteniendo la respuesta",
                "respuestas": ["Si","No","Con Ayuda"] 
            },
            {
                "pregunta": "Estimación de los niveles de comprension y motivacion del niño con respecto al primer día",
                "respuestas": [1,2,3,4,5,6,7,8,9,10] 
            },
            {
                "pregunta": "Nivel de evolucion de la semana",
                "respuestas": [1,2,3,4,5,6,7,8,9,10] 
            },
            {
                "pregunta": "Cuanto crees que has aprendido sobre la forma de pensar de tu hijo?",
                "respuestas": [1,2,3,4,5,6,7,8,9,10] 
            }
        ]
    }


]
*/
