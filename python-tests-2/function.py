from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import cv2
import pytesseract
import os
from gingerit.gingerit import GingerIt


def translate_image_text(image_path): 
    im = np.array(Image.open(image_path))
    plt.figure(figsize=(10,10))
    plt.title('PLAIN IMAGE')
    plt.imshow(im); plt.xticks([]); plt.yticks([])
    plt.savefig('1_PLAIN_IMAGE.png')
    text = pytesseract.image_to_string(im)    
    im= cv2.bilateralFilter(im,5, 55,60)
    plt.figure(figsize=(10,10))
    plt.title('BILATERAL FILTER')
    plt.imshow(im); plt.xticks([]); plt.yticks([])
    plt.savefig('2_BILATERAL_FILTER.png',bbox_inches='tight')
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    plt.figure(figsize=(10,10))
    plt.title('GRAYSCALE IMAGE')
    plt.imshow(im, cmap='gray'); plt.xticks([]); plt.yticks([])
    plt.savefig('3_GRAYSCALE_IMAGE.png',bbox_inches='tight')
    _, im = cv2.threshold(im, 240, 255, 1) 
    plt.figure(figsize=(10,10))
    plt.title('IMMAGINE BINARIA')
    plt.imshow(im, cmap='gray'); plt.xticks([]); plt.yticks([])
    plt.savefig('4_IMMAGINE_BINARIA.png',bbox_inches='tight')
    custom_config = r"--oem 3 --psm 11 -c tessedit_char_whitelist= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ '"
    text = pytesseract.image_to_string(im, lang='eng', config=custom_config)
    print(text.replace('\n', ''))

def preprocess_finale(im):
    im= cv2.bilateralFilter(im,5, 55,60)
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    _, im = cv2.threshold(im, 240, 255, 1)
    return im

def main():
    # Get the current directory of the script
    current_dir = os.path.dirname(os.path.abspath(__file__))

    # Load the image from the current directory
    image_name = "input_image.jpg"
    image_path = os.path.join(current_dir, image_name)

    # Translate the text in the image and save the translated image
    #translate_image_text(image_path)
    img=np.array(Image.open(image_path))
    im=preprocess_finale(img)
    custom_config = r"--oem 3 --psm 11 -c tessedit_char_whitelist= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ '"
    text = pytesseract.image_to_string(im, lang='eng', config=custom_config)

    text = text.replace('\n', '')
    
    parser = GingerIt()
    parsed = parser.parse(text)
    print(parsed['result'])
    


if __name__ == "__main__":
    main()